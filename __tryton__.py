#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Sale Reporting',
    'name_de_DE': 'Verkauf Auswertung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz/',
    'description': '''Reporting module for Sales
    Provides reportings for
    - Sales per product per period
    - Sales per party per period
''',
    'description_de_DE': '''Auswertungsmodul für Verkäufe
    Ermöglicht Auswertungen für
    - Verkäufe pro Artikel pro Zeitraum
    - Verkäufe pro Partei pro Zeitraum
''',
    'depends': [
        'sale',
    ],
    'xml': [
        'sale.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
