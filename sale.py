#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.wizard import Wizard
from trytond.pyson import PYSONEncoder
from trytond.transaction import Transaction
from trytond.pool import Pool



class SalesProduct(ModelSQL, ModelView):
    'Sales per product for specific period'
    _name = 'sale.sales_product'
    _description = __doc__

    product = fields.Many2One('product.product', 'Product')
    quantity = fields.Float('Quantity',
            digits=(16, 2))
    amount = fields.Float('Amount',
            digits=(16, 2))

    def table_query(self):
        clause = ' '
        args = []
        if Transaction().context.get('party'):
            clause += 'AND sale_sale.party = %s '
            args.append(Transaction().context['party'])
        if Transaction().context.get('start_date'):
            clause += 'AND sale_sale.sale_date >= %s '
            args.append(Transaction().context['start_date'])
        if Transaction().context.get('end_date'):
            clause += 'AND sale_sale.sale_date <= %s '
            args.append(Transaction().context['end_date'])
        return ('SELECT DISTINCT(sale_line.product) AS id, ' \
                    'MAX(sale_line.create_uid) AS create_uid, ' \
                    'MAX(sale_line.create_date) AS create_date, ' \
                    'MAX(sale_line.write_uid) AS write_uid, ' \
                    'MAX(sale_line.write_date) AS write_date, ' \
                    'product, ' \
                    'SUM(COALESCE(sale_line.quantity, 0)) AS quantity, ' \
                    'SUM(COALESCE(sale_line.unit_price, 0)) AS unit_price, ' \
                    'SUM(COALESCE(sale_line.quantity * sale_line.unit_price, 0)) AS amount '
                'FROM sale_line, sale_sale ' \
                'WHERE True ' \
                'AND sale_line.sale = sale_sale.id '\
                "AND sale_sale.state IN ('confirmed','done') "\
                + clause + \
                'GROUP BY product', args)

SalesProduct()


class SalesParty(ModelSQL, ModelView):
    'Sales per party for specific period'
    _name = 'sale.sales_party'
    _description = __doc__

    party = fields.Many2One('party.party', 'Party')
    quantity = fields.Float('Quantity',
            digits=(16, 2))
    amount = fields.Float('Amount',
            digits=(16, 2))

    def table_query(self):
        clause = ' '
        args = []
        if Transaction().context.get('product'):
            clause += 'AND sale_line.product = %s '
            args.append(Transaction().context['product'])
        if Transaction().context.get('start_date'):
            clause += 'AND sale_sale.sale_date >= %s '
            args.append(Transaction().context['start_date'])
        if Transaction().context.get('end_date'):
            clause += 'AND sale_sale.sale_date <= %s '
            args.append(Transaction().context['end_date'])
        return ('SELECT DISTINCT(sale_sale.party) AS id, ' \
                    'MAX(sale_line.create_uid) AS create_uid, ' \
                    'MAX(sale_line.create_date) AS create_date, ' \
                    'MAX(sale_line.write_uid) AS write_uid, ' \
                    'MAX(sale_line.write_date) AS write_date, ' \
                    'party, ' \
                    'SUM(COALESCE(sale_line.quantity, 0)) AS quantity, ' \
                    'SUM(COALESCE(sale_line.quantity * sale_line.unit_price, 0)) AS amount '
                'FROM sale_line, sale_sale ' \
                'WHERE True ' \
                'AND sale_line.sale = sale_sale.id '\
                "AND sale_sale.state IN ('confirmed','done') "\
                + clause + \
                'GROUP BY party', args)

SalesParty()


class SalesProductInit(ModelView):
    'Sales per Product'
    _name = 'sale.sales_product.init'
    _description = __doc__
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    party = fields.Many2One('party.party', 'Party')

SalesProductInit()


class SalesPartyInit(ModelView):
    'Sales per Party'
    _name = 'sale.sales_party.init'
    _description = __doc__
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    product = fields.Many2One('product.product', 'Product')

SalesPartyInit()


class OpenSalesProduct(Wizard):
    'Open Sales per Product'
    _name = 'sale.open_sales_product'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'sale.sales_product.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('open', 'Open', 'tryton-ok', True),
                ],
            },
        },
        'open': {
            'result': {
                'type': 'action',
                'action': '_action_open',
                'state': 'end',
            },
        },
    }

    def _action_open(self, data):
        model_data_obj = Pool().get('ir.model.data')
        act_window_obj = Pool().get('ir.action.act_window')

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_sales_product_view_form'),
            ('module', '=', 'sale_reporting'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['pyson_context'] = PYSONEncoder().encode({
                'start_date': data['form']['start_date'],
                'end_date': data['form']['end_date'],
                'party': data['form']['party']})
        return res

OpenSalesProduct()


class OpenSalesParty(Wizard):
    'Open Sales per Party'
    _name = 'sale.open_sales_party'
    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'sale.sales_party.init',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('open', 'Open', 'tryton-ok', True),
                ],
            },
        },
        'open': {
            'result': {
                'type': 'action',
                'action': '_action_open',
                'state': 'end',
            },
        },
    }

    def _action_open(self, data):
        model_data_obj = Pool().get('ir.model.data')
        act_window_obj = Pool().get('ir.action.act_window')

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_sales_party_view_form'),
            ('module', '=', 'sale_reporting'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['pyson_context'] = PYSONEncoder().encode({
            'start_date': data['form']['start_date'],
            'end_date': data['form']['end_date'],
            'product': data['form']['product'],
            })
        return res

OpenSalesParty()
